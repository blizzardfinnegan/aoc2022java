package day03;

import helpers.FileRead;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;


/**
 * DayThree, PartOne of Advent of Code 2022
 *
 * Input file contains lines of semi-random strings of characters.
 * Each character is a letter, and the letter corresponds to a priority value.
 * Lower-case has priorities 1 thru 26 (a=1, b=2, etc), uppercase has priorities
 * 27 thru 52.
 *
 * There will be a single item that exists in each grouping of 3 lines.
 * 
 * Find the sum of the priorities of the repeat items.
 *
 * @author Blizzard Finnegan
 */

public class DayThreePartTwo 
{
  public static void main(String[] args) throws Error 
  {
    List<Set<Character>> firstLineList = new ArrayList<>();
    List<Set<Character>> secondLineList = new ArrayList<>();
    List<Set<Character>> thirdLineList = new ArrayList<>();
    List<Character> bothHalves = new LinkedList<>();

    List<String> fileInput = FileRead.readFile("day03/data/input.txt");

    int counter = 0;
    for (String line : fileInput) 
    {
      Set<Character> lineSet = new HashSet<>();

      for(char letter : line.toCharArray()) 
      {
        lineSet.add((Character)letter);
      }

      switch (counter)
      {
        case 0:
          firstLineList.add(lineSet);
          break;
        case 1:
          secondLineList.add(lineSet);
          break;
        case 2:
          thirdLineList.add(lineSet);
          break;
        default:
          System.out.println("ERROR!!!");
          break;
      }
      counter = (counter + 1) % 3;
    }

    for(int i = 0; i < firstLineList.size(); i++)
    {
      Set<Character> firstSet = firstLineList.get(i);
      Set<Character> secondSet = secondLineList.get(i);
      Set<Character> thirdSet = thirdLineList.get(i);

      for(Character character : firstSet)
      {
        if(secondSet.contains(character) && thirdSet.contains(character))
        {
          bothHalves.add(character);
        }
      }
    }

    int finalValue = 0;

    for(Character charObject : bothHalves)
    {
      char pair = charObject.charValue();
      if(Character.isUpperCase(pair)) 
        finalValue += pair - 'A' + 26 + 1;
      else
        finalValue += pair - 'a' + 1;
    }

    System.out.println("Sum of priorities: " + finalValue);
  }
}
