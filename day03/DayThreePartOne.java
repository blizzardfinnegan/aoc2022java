package day03;

import helpers.FileRead;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;


/**
 * DayThree, PartOne of Advent of Code 2022
 *
 * Input file contains lines of semi-random strings of characters.
 * Each character is a letter, and the letter corresponds to a priority value.
 * Lower-case has priorities 1 thru 26 (a=1, b=2, etc), uppercase has priorities
 * 27 thru 52.
 *
 * Each line can be split in half. There will be a single item that exists in
 * both halves of the line.
 * 
 * Find the sum of the priorities of the repeat items.
 *
 * @author Blizzard Finnegan
 */

public class DayThreePartOne 
{
  public static void main(String[] args) throws Error 
  {
    List<List<Character>> firstHalfList = new ArrayList<>();
    List<List<Character>> secondHalfList = new ArrayList<>();
    List<Character> bothHalves = new LinkedList<>();

    List<String> fileInput = FileRead.readFile("day03/data/input.txt");

    for (String line : fileInput) 
    {
      String firstHalf = line.substring(0,(line.length()/2));
      String secondHalf = line.substring((line.length()/2),line.length());

      List<Character> firstHalfInner = new ArrayList<>();
      List<Character> secondHalfInner = new ArrayList<>();

      for(char letter : firstHalf.toCharArray()) 
      {
        firstHalfInner.add((Character)letter);
      }
      for(char letter : secondHalf.toCharArray()) 
      {
        secondHalfInner.add((Character)letter);
      }

      firstHalfList.add(firstHalfInner);
      secondHalfList.add(secondHalfInner);
    }

    for (int index = 0; index < firstHalfList.size(); index++)
    {
      List<Character> firstHalfLine = firstHalfList.get(index);
      List<Character> secondHalfLine = secondHalfList.get(index);
      Set<Character> firstHalfSet = new HashSet<>();
      Set<Character> secondHalfSet = new HashSet<>();

      for(Character firstChar : firstHalfLine)
      {
        firstHalfSet.add(firstChar);
      }

      for(Character secondChar : secondHalfLine)
      {
        secondHalfSet.add(secondChar);
      }

      for(Character firstHalfChar : firstHalfSet)
      {
        if(secondHalfSet.contains(firstHalfChar))
        {
          bothHalves.add(firstHalfChar);
          break;
        }
      }
    }

    int finalValue = 0;

    for(Character charObject : bothHalves)
    {
      char pair = charObject.charValue();
      if(Character.isUpperCase(pair)) 
        finalValue += pair - 'A' + 26 + 1;
      else
        finalValue += pair - 'a' + 1;
    }

    System.out.println("Sum of priorities: " + finalValue);
  }
}
