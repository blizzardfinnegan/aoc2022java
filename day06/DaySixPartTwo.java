package day06;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import helpers.*;

/**
 * DaySix, PartTwo of Advent of Code 2022
 *
 * Input is a single line of seemingly-random 
 * string of characters. Find the first instance 
 * of 14 unique characters in a row.
 *
 * @author Blizzard Finnegan
 */
public class DaySixPartTwo
{
	public static int arraySize = 14;
	public static void main(String[] args) throws Error
	{
		List<String> fileRead = FileRead.readFile("day06/data/input.txt");
		String inputString = fileRead.get(0);
		char[] check = new char[arraySize];
		for(int i = 0; i < inputString.length(); i++)
		{
			if(i == 0)
			{
				for(int j = i; j < arraySize; j++)
				{
					check[j] = inputString.charAt(i);
				}
				continue;
			}
			else
			{
				for(int j = 0; j < arraySize - 1; j++)
				{
					check[j] = check[j+1];
				}
				check[arraySize-1] = inputString.charAt(i);
				if(i < arraySize) continue;
			}
			if(individuality(check))
			{
				System.out.println(Arrays.toString(check));
				System.out.println(i+1);
				break;
			}
		}
	}

	/**
	 * Check if all values in char array are unique
	 * @return boolean
	 */
	public static boolean individuality(char[] check)
	{
		for(int i = 0; i < arraySize; i++)
		{
			for(int j = 0; j < arraySize; j++)
			{
				if(i == j) continue;
				if(check[i] == check[j]) return false;
			}
		}
		return true;
	}
}
