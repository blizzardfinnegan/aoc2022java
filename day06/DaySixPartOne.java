package day06;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import helpers.*;

/**
 * DaySix, PartOne of Advent of Code 2022
 *
 * Input is a single line of seemingly-random 
 * string of characters. Find the first instance 
 * of four unique characters in a row.
 *
 * @author Blizzard Finnegan
 */
public class DaySixPartOne
{
	public static void main(String[] args) throws Error
	{
		List<String> fileRead = FileRead.readFile("day06/data/input.txt");
		String inputString = fileRead.get(0);
		char[] check = new char[4];
		for(int i = 0; i < inputString.length(); i++)
		{
			if(i == 0)
			{
				for(int j = i; j < 4; j++)
				{
					check[j] = inputString.charAt(i);
				}
				continue;
			}
			else
			{
				check[0] = check[1];
				check[1] = check[2];
				check[2] = check[3];
				check[3] = inputString.charAt(i);
				if(i < 4) continue;
			}
			if(individuality(check))
			{
				System.out.println(Arrays.toString(check));
				System.out.println(i+1);
				break;
			}
		}
	}

	/**
	 * Check if all values in char array are unique
	 * @return boolean
	 */
	public static boolean individuality(char[] check)
	{
		char zero = check[0];
		char one = check[1];
		char two = check[2];
		char three = check[3];

		boolean output = true;
		output = output && zero  != one;
		output = output && zero  != two;
		output = output && zero  != three;
		output = output && one   != two;
		output = output && one   != three;
		output = output && two   != three;
		return output;
	}
}
