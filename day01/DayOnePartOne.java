package day01;

import helpers.SimpleChecks;
import helpers.FileRead;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * DayOne, PartOne of Advent of Code 2022
 *
 * The input and test input files given consist of lines of text, as usual.
 * These lines of text are either a singular number, or a blank line.
 * Each grouping of numbers represents an elf, the numbers themselves representing
 * calorie count in the food they carry. 
 *
 * Determine the elf with the most amount of calories, and print the number of calories.
 *
 * @author Blizzard Finnegan
 */
public class DayOnePartOne
{
	public static void main(String[] args) throws Error
	{
		//Variable creation
		//Key = Calories; value = elfNumber;
		//Using a TreeMap means that all values in the map are organised from most calories to least.
		//This could also be done with TreeSet instead, but it was unclear if the name/number of the elf would be important in PartTwo.
		TreeMap<Integer, Integer> elves = new TreeMap<>();
		int elfNumber = 1;
		int calorieCount = 0;
		
		
		//Import the given input file
		List<String> fileInput = FileRead.readFile("day01/data/input.txt");

		//Iterate through file
		for(String line : fileInput)
		{
			//If the line is an integer, add it to the calorie total
			if(SimpleChecks.isInteger(line)) calorieCount += Integer.parseInt(line);

			//If the line isn't an integer, add the elf to the map, increment the elfNumber, and reset the calorie count
			else
			{
				elves.put(calorieCount,elfNumber);
				calorieCount = 0;
				elfNumber++;
			}
		}
		//Save the number of the elf who has the most calories
		String finalElf = elves.lastEntry().getValue().toString();
		//Save the number of calories the above elf has
		String finalCalorieCount = elves.lastEntry().getKey().toString();

		//final print
		System.out.println("Elf #" + finalElf + " has the most calories, at " + finalCalorieCount);
	}
}
