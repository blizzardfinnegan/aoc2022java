package day01;

import helpers.SimpleChecks;
import helpers.FileRead;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * DayOne, PartTwo of Advent of Code 2022
 *
 * The input and test input files given consist of lines of text, as usual.
 * These lines of text are either a singular number, or a blank line.
 * Each grouping of numbers represents an elf, the numbers themselves representing
 * calorie count in the food they carry. 
 *
 * Determine the collective amount of calories carried by the top 3 elves.
 * 
 * @author Blizzard Finnegan
 */
public class DayOnePartTwo
{
	//throws Error is to handle any errors created by FileRead and SimpleChecks
	public static void main(String[] args) throws Error
	{
		//Variable creation
		//Key = Calories; value = elfNumber;
		//Using a TreeMap means that all values in the map are organised from most calories to least.
		//This could also be done with TreeSet instead, however this is copied from PartOne, and minimal modification was wanted
		TreeMap<Integer, Integer> elves = new TreeMap<>();
		int elfNumber = 1;
		int calorieCount = 0;

		//Import the given input file
		List<String> fileInput = FileRead.readFile("day01/data/input.txt");

		//Iterate through file
		for(String line : fileInput)
		{
			//If the line is an integer, add it to the calorie total
			if(SimpleChecks.isInteger(line)) calorieCount += Integer.parseInt(line);

			//If the line isn't an integer, add the elf to the map, increment the elfNumber, and reset the calorie count
			else
			{
				elves.put(calorieCount,elfNumber);
				calorieCount = 0;
				elfNumber++;
			}
		}

		//Create a variable to save the final calorie count for the final print
		int finalCalorieCount = 0;

		//Remove the 3 biggest calorie numbers, and add them to the above variables
		for(int i = 0; i < 3; i++)
		{
			finalCalorieCount += elves.pollLastEntry().getKey();
		}

		//final print
		System.out.println("The top 3 elves carry " + finalCalorieCount + " calories");
	}
}
