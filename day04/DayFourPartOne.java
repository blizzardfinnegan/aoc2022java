package day04;

import java.util.List;

import helpers.*;
/**
 * DayFour, PartOne of Advent of Code 2022
 * Input is pairs of numbers, like so:
 *
 * 	2-6,4-8
 *
 * find the pairs that entirely contain each other. 
 * For  example, 6-6 is contained entirely within 4-6, and 2-8 contains 3-7.
 *
 * @author Blizzard Finnegan
 */
public class DayFourPartOne
{
	public static void main(String[] args) throws Error
	{
		//Counter for number of overlapping pairs
		int counter = 0;
		//Read in the file
		List<String> fileInput = FileRead.readFile("day04/data/input.txt");
		//For each line in the file...
		for(String line : fileInput)
		{
			//Each line of the file is a pair.
			//Split each line into an array of a start and end character
			String[] halves = line.split(",");
			String[] first = halves[0].split("-");
			String[] second = halves[1].split("-");

			//Make the above arrays into ints, for easier math
			int[] firstInt = new int[2];
			int[] secondInt = new int[2];
			for(int i = 0; i < 2; i++)
			{
				if(SimpleChecks.isInteger(first[i]))
					firstInt[i] = Integer.parseInt(first[i]);
				if(SimpleChecks.isInteger(second[i]))
					secondInt[i] = Integer.parseInt(second[i]);
			}

			
			if(
					//If the second pair is inside the first...
					(between(firstInt[0], secondInt[0], firstInt[1])
				&& between(firstInt[0], secondInt[1], firstInt[1]))
					//or the first pair is inside the second...
			  ||(between(secondInt[0], firstInt[0], secondInt[1])
			  && between(secondInt[0], firstInt[1], secondInt[1])))
			{
				//increment the counter
				counter++;
			}
		}

		System.out.println(counter);
	}

	public static boolean between(int min, int variable, int max)
	{ return variable >= min && variable <= max; }
}
