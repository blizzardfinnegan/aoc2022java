package day02;

import helpers.SimpleChecks;
import helpers.FileRead;
import helpers.Pair;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


/**
 * DayTwo, PartOne of Advent of Code 2022
 *
 * Input file contains lines of Rock Paper Scissors games.
 * A/X = Rock
 * B/Y = Paper 
 * C/Z = Scissors
 * A/B/C is the move of the opponent, X/Y/Z is your move.
 *
 * This is in the format:
 *
 * A Y
 * B X
 * C Z
 *
 * Score of the round is calculated as sum of two numbers based on the move used, and the outcome of the round:
 * Rock     = 1 point
 * Paper    = 2 points
 * Scissors = 3 points
 * ---------------------
 * Win      = 6 points
 * Draw     = 3 points
 * Loss     = 0 points
 *
 * Calculate the number of points earned in the given input file. (testInput file is also provided, as normal)
 *
 * @author Blizzard Finnegan
 */
public class DayTwoPartOne
{
	public static void main(String[] args) throws Error
	{
		//List of Move pairs
		List<Pair<Move,Move>> roundList = new LinkedList<>();

		int pointTotal = 0;

		//File, imported as a list
		List<String> fileInput = FileRead.readFile("day02/data/input.txt");

		//Iterate over the list
		for(String line : fileInput)
		{
			//Get your move
			Move yourMove = charToMove(line.charAt(2));
			//get your opponents move
			Move opponentMove = charToMove(line.charAt(0));
			//Add the move pair to the list of moves
			roundList.add(new Pair<Move,Move>(yourMove,opponentMove));
		}

		//Iterate over the list of moves, calculate the total number of points
		for(Pair<Move,Move> round : roundList)
		{
			pointTotal += roundCalc(round.getSecond(), round.getFirst());
		}
		System.out.println("Point Total: " + pointTotal);
	}

	//Convert a character to a Move
	private static Move charToMove(char input)
	{
		switch(input)
		{
			case 'A':
			case 'X':
				return Move.ROCK;
			case 'B':
			case 'Y':
				return Move.PAPER;
			case 'C':
			case 'Z':
				return Move.SCISSORS;
		}
		return null;
	}

	//Calculate the points, given two moves
	private static int roundCalc(Move opponentMove, Move yourMove)
	{
		int points = yourMove.points();
		switch(yourMove)
		{
			case ROCK:
				switch(opponentMove)
				{
					case ROCK:     return (points + 3);
					case PAPER:    return (points + 0);
					case SCISSORS: return (points + 6);
				}
			case PAPER:
				switch(opponentMove)
				{
					case ROCK:     return (points + 6);
					case PAPER:    return (points + 3);
					case SCISSORS: return (points + 0);
				}
			case SCISSORS:
				switch(opponentMove)
				{
					case ROCK:     return (points + 0);
					case PAPER:    return (points + 6);
					case SCISSORS: return (points + 3);
				}
		}
		return -1;
	}

	//Possible moves, and their point values
	static enum Move 
	{
		ROCK(1,"Rock"),
		PAPER(2,"Paper"),
		SCISSORS(3,"Scissors");
	
		private int pointValue;
		private String name;
	
		private Move(int pointValue, String name) { this.pointValue = pointValue; this.name = name; }
		protected int points() { return this.pointValue; }
		public String toString() { return this.name; }
	}
}

