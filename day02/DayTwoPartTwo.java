package day02;

import helpers.SimpleChecks;
import helpers.FileRead;
import helpers.Pair;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


/**
 * DayTwo, PartOne of Advent of Code 2022
 *
 * Input file contains lines of Rock Paper Scissors games.
 * A = Opponent plays Rock
 * B = Opponent plays Paper 
 * C = Opponent plays Scissors
 * X = You need to lose
 * Y = You need to draw
 * Z = You need to win
 *
 * This is in the format:
 *
 * A Y
 * B X
 * C Z
 *
 * Score of the round is calculated as sum of two numbers based on the move YOU used, and the outcome of the round:
 * Rock     = 1 point
 * Paper    = 2 points
 * Scissors = 3 points
 * ---------------------
 * Win      = 6 points
 * Draw     = 3 points
 * Loss     = 0 points
 *
 * Calculate the number of points earned in the given input file. (testInput file is also provided, as normal)
 *
 * @author Blizzard Finnegan
 */
public class DayTwoPartTwo
{
	private static HashMap<Character,Move> moveMap = new HashMap<>();

	static
	{
		moveMap.put('A',Move.ROCK);
		moveMap.put('B',Move.PAPER);
		moveMap.put('C',Move.SCISSORS);
	}

	public static void main(String[] args) throws Error
	{
		//List of Move pairs
		List<Pair<Move,Move>> roundList = new LinkedList<>();

		int pointTotal = 0;

		//File, imported as a list
		List<String> fileInput = FileRead.readFile("day02/data/input.txt");

		//Iterate over the list
		for(String line : fileInput)
		{
			//get your opponents move
			Move opponentMove = charToTheirMove(line.charAt(0));
			//Get your move
			Move yourMove = charToYourMove(line.charAt(2),opponentMove);
			//Add the move pair to the list of moves
			roundList.add(new Pair<Move,Move>(yourMove,opponentMove));
		}

		//Iterate over the list of moves, calculate the total number of points
		for(Pair<Move,Move> round : roundList)
		{
			pointTotal += roundCalc(round.getSecond(), round.getFirst());
		}
		System.out.println("Point Total: " + pointTotal);
	}

	//Convert a character to a Move
	private static Move charToTheirMove(char input)
	{
		return moveMap.get(input);
	}

	private static Move charToYourMove(char input, Move opponentMove)
	{
		switch(input)
		{
			//X = lose, Y = draw, Z = win
			case 'X':
				switch(opponentMove)
				{
					case ROCK:     return Move.SCISSORS;
					case PAPER:    return Move.ROCK;
					case SCISSORS: return Move.PAPER;
				}
			case 'Y':
				return opponentMove;
			case 'Z':
				switch(opponentMove)
				{
					case ROCK:     return Move.PAPER;
					case PAPER:    return Move.SCISSORS;
					case SCISSORS: return Move.ROCK;
				}
		}
		return null;
	}

	//Calculate the points, given two moves
	private static int roundCalc(Move opponentMove, Move yourMove)
	{
		int points = yourMove.points();
		switch(yourMove)
		{
			case ROCK:
				switch(opponentMove)
				{
					case ROCK:     return (points + 3);
					case PAPER:    return (points + 0);
					case SCISSORS: return (points + 6);
				}
			case PAPER:
				switch(opponentMove)
				{
					case ROCK:     return (points + 6);
					case PAPER:    return (points + 3);
					case SCISSORS: return (points + 0);
				}
			case SCISSORS:
				switch(opponentMove)
				{
					case ROCK:     return (points + 0);
					case PAPER:    return (points + 6);
					case SCISSORS: return (points + 3);
				}
		}
		return -1;
	}

	//Possible moves, and their point values
	static enum Move 
	{
		ROCK(1,"Rock"),
		PAPER(2,"Paper"),
		SCISSORS(3,"Scissors");
	
		private int pointValue;
		private String name;
	
		private Move(int pointValue, String name) { this.pointValue = pointValue; this.name = name; }
		protected int points() { return this.pointValue; }
		public String toString() { return this.name; }
	}
}

