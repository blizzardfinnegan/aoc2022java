package day05;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

import helpers.*;
/**
 * DayFive, PartTwo of Advent of Code 2022
 * Input is a series of stacks, followed by move instructions.
 * Follow the move instructions, noting that this time, all boxes move as a sub-stack.
 * The final output should be the top box of each stack.
 * @author Blizzard Finnegan
 */
public class DayFivePartTwo
{
	public static ArrayList<Deque<Character>> allStacks = new ArrayList<>();
	public static ArrayList<Deque<Character>> sidewaysStacks = new ArrayList<>();

	public static void main(String[] args) throws Error
	{
		//Read in the file
		List<String> fileInput = FileRead.readFile("day05/data/input.txt");
		//Iterate over file
		for(String line : fileInput)
		{
			if(line.length() <= 1)
				continue;
			//Only do the below for the stack stuff
			else if(line.charAt(1) != '1' && line.charAt(0) != 'm')
			{
				//Trim off first character, to make indexing easier.
				line = line.substring(1);

				//Create a stack
				Deque<Character> layerOfStack = new LinkedList<>();

				//Iterate over important characters
				for(int i = 0; i < line.length(); i+=4)
				{
					//Save them to a stack
					layerOfStack.addLast((Character)line.charAt(i));
				}
				//Save the layer of the stack
				sidewaysStacks.add(layerOfStack);
			}
			else if(line.charAt(1) == '1')
			{
				//Get the size of the lowest layer of the stack
				int size = sidewaysStacks.get(sidewaysStacks.size() - 1).size();

				//Populate allStacks with the right number of stacks
				for(int i = 0; i < size; i++)
				{
					allStacks.add(new LinkedList<>());
				}

				//Now that the boxes have all been imported, time to PivotTables
				for(int i = 0; i < size; i++)
				{
					Deque<Character> stack = allStacks.get(i);
					for(Deque<Character> layer : sidewaysStacks)
					{
						Character value = layer.removeFirst();
						if(!Character.isSpaceChar((char)value))
						{
							stack.addLast(value);
						}
					}
				}
			}
			else if(line.charAt(0) == 'm')
			{
				String[] rawSplitLine = line.split(" ");
				int moveCount = Integer.parseInt(rawSplitLine[1]);
				int sourceStack = Integer.parseInt(rawSplitLine[3]);
				int destStack = Integer.parseInt(rawSplitLine[5]);
				
				stackShift(moveCount,sourceStack,destStack);
			}
		}
		for(Deque<Character> stack : allStacks)
		{
			System.out.print(stack.getFirst().toString());
		}
	}

	public static void stackShift(int moveCount, int sourceStack, int destStack)
	{
		Deque<Character> source = allStacks.get(sourceStack - 1);
		Deque<Character> dest = allStacks.get(destStack - 1);
		Stack<Character> middle = new Stack<>();
		for(int i = 0; i < moveCount; i++)
		{
			middle.push(source.removeFirst());
		}
		for(int i = 0; i < moveCount; i++)
		{
			dest.addFirst(middle.pop());
		}
	}
}
