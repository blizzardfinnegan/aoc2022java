package day08;

import java.util.ArrayList;
import java.util.List;

import helpers.*;

/**
 * DayEight, PartOne of Advent of Code 2022
 *
 * The input file contains a grid of numbers. 
 * Each number denotes a height of a tree. 
 * Count the number of trees visible from any edge.
 *
 * @author Blizzard Finnegan
 */
public class DayEightPartOne
{
	public static void main(String[] args) throws Error
	{
		ArrayList<ArrayList<Integer>> grid = new ArrayList<>();
		List<String> fileRead = FileRead.readFile("day08/data/testInput.txt");

		for(String line : fileRead)
		{
			ArrayList<Integer> intLine = new ArrayList<>();
			char[] tempChar = line.toCharArray();
			for(int i = 0; i < line.length(); i++)
			{
				intLine.add(tempChar[i] - '0');
			}
			grid.add(intLine);
		}
	}
}
