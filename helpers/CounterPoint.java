package helpers;

import java.util.ArrayList;
import java.util.List;

/**
 * A helper class containing a cartesian point, and a counter value.
 * 
 * @author Blizzard Finnegan
 */
public class CounterPoint
{
	private int counter;
	//private boolean rolledOver;
	private final IntPair location;
	public CounterPoint(IntPair location, int counter)
	{
		this.counter = counter;
		this.location = location;
	}

	public int getCount() { return this.counter; }
	public void increment() { this.counter++; }
	public boolean rollOver() 
	{ 
		boolean output = this.counter > 9;
		if(output) this.counter = 0;
		return output;
	}
	public IntPair getLocation() { return this.location; }

	public List<IntPair> getNeighbors(int xMax, int yMax)
	{
		List<IntPair> neighbors = new ArrayList<IntPair>();
		if(this.getX() > 0) 
		{
			neighbors.add(new IntPair(this.getX() - 1, this.getY()));
			if(this.getY() > 0) neighbors.add(new IntPair(this.getX() - 1, this.getY() - 1));
			if(this.getY() < yMax - 1) neighbors.add(new IntPair(this.getX() - 1, this.getY() + 1));
		}
		if(this.getX() < xMax - 1)
		{
			neighbors.add(new IntPair(this.getX() + 1, this.getY()));
			if(this.getY() > 0) neighbors.add(new IntPair(this.getX() + 1, this.getY() - 1));
			if(this.getY() < yMax - 1) neighbors.add(new IntPair(this.getX() + 1, this.getY() + 1));
		}
		if(this.getY() > 0) neighbors.add(new IntPair(this.getX(), this.getY() - 1));
		if(this.getY() < yMax - 1) neighbors.add(new IntPair(this.getX(), this.getY() + 1));
		return neighbors;
	}
	public int getX() { return this.location.getX(); }
	public int getY() { return this.location.getY(); }

	@Override
	public boolean equals(Object obj) 
	{
		if(!(obj instanceof CounterPoint)) return false;
		CounterPoint castObj = (CounterPoint)obj;
		return this.counter == castObj.counter && this.location.equals(castObj.location);
	}

	@Override
	public String toString() 
	{
		return String.format("{%s, %s}", this.location.toString(), this.counter);
	}
}
