package helpers;

import java.util.List;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.LinkedList;

/**
 * A helper class designed to convert a plaintext file into
 * a native Java format for easy interaction.
 *
 * @author Blizzard Finnegan
 */
public class FileRead
{
  /**
   * Convert a given file location into a series of strings
   *
   * @param String filename - the location of the file
   *
   * @return List<String> - Returns a {@link java.util.List} of Strings, as
   *                        they exist in the given file.
   *                        Returns null if the given file doesn't exist.
   */
  public static List<String> readFile(String filename)
  {
    List<String> output = new LinkedList<>();
    File file = new File(filename);
    if(!file.exists())
    {
      System.out.println(filename);
      System.out.println("File doesn't exist!");
      return null;
    }
    try (FileReader fileReader = new FileReader(file);
         BufferedReader buffReader = new BufferedReader(fileReader);)
    {
      String readLineContents = buffReader.readLine();
      while(readLineContents != null)
      {
        output.add(readLineContents);
        readLineContents = buffReader.readLine();
      }
    }
    catch(Exception e)
    {
      System.out.println("Something broke...");
      System.out.println(e);
    }
    return output;
  }
}
