package helpers;

/**
 * A helper class containing a Cartesian point, and a weight for the point.
 * This class is read-only.
 *
 * @author Blizzard Finnegan
 */
public class WeightedPoint
{
	private final int weight;
	private final IntPair location;
	public WeightedPoint(IntPair location, int weight)
	{
		this.weight = weight;
		this.location = location;
	}

	public int getWeight() { return this.weight; }
	public IntPair getLocation() { return this.location; }
	public int getX() { return this.location.getX(); }
	public int getY() { return this.location.getY(); }

	@Override
	public boolean equals(Object obj) 
	{
		if(!(obj instanceof WeightedPoint)) return false;
		WeightedPoint castObj = (WeightedPoint)obj;
		return this.weight == castObj.weight && this.location.equals(castObj.location);
	}

	@Override
	public String toString() 
	{
		return String.format("{%s, %s}", this.location.toString(), this.weight);
	}
}
