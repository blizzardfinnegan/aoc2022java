package helpers;

import java.util.Scanner;

/**
 * Simple checks to make life easier, so I don't have to
 * continually rewrite these classes.
 *
 * @author Blizzard Finnegan
 */
public class SimpleChecks
{
  /**
   * Check to see if a String, by itself, can be parsed into a valid integer
   *
   * @param String input - The String to test
   * @param int radix - The base number system to use. (Base2 or Binary, Base8 or octal, etc)
   *
   * @return boolean - Whether the String
   */
  public static boolean isInteger(String input, int radix)
  {
    Scanner sc = new Scanner(input.trim());
    if(!sc.hasNextInt(radix)) return false;
    sc.nextInt(radix);
    return !sc.hasNext();
  }

  /**
   * Check to see if a String, by itself, can be parsed into a valid integer.
   * Excludes the radix argument; base10 (decimal) is assumed.
   *
   * @param String input - The String to test
   *
   * @return boolean - Whether the String
   */
  public static boolean isInteger(String input) { return SimpleChecks.isInteger(input,10); }

  /**
   * Convert an String to an integer
   *
   * @param String input - the String to test
   * @return int - the string, converted to an int
   * 
   * Will throw an error if the string is not an int
   */
  public static int stringToInt(String input)
  {
    if(!SimpleChecks.isInteger(input)) 
    {
      throw new Error(input + "is not an integer!");
    }
    else return Integer.parseInt(input);
  }
}
