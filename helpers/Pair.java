package helpers;

/**
 * Helper class containing some Pair.
 * These values are read-only once created.
 *
 * @author Blizzard Finnegan
 */
public class Pair<T, E> {
	private final T firstVal;
	private final E secondVal;

	public Pair(T first, E second) { this.firstVal = first; this.secondVal = second; }

	public T getFirst() { return this.firstVal; }
	public E getSecond() { return this.secondVal; }

	@Override
	public String toString() { return String.format("(%s,%s)", this.firstVal.toString(), this.secondVal.toString());}
}

