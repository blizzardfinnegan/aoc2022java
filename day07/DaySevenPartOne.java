package day07;

import java.util.List;

import helpers.*;

/**
 * DaySeven, PartOne of Advent of Code
 *
 * input is a series of commands and their outputs.
 * Find all of the directories whose size is at most 100,000. Then, calculate their sum.
 *
 * @author Blizzard Finnegan
 */
public class DaySevenPartOne
{
	public static void main(String[] args) throws Error
	{
		List<String> fileRead = FileRead.readFile("day07/data/testInput.txt");

		for(String line : fileRead)
		{
		}
	}
}
